import express from 'express';
import helmet from 'helmet';
import compression from 'compression';
import cors from 'cors';
const morgan = require('morgan');

import {
    uploadMiddleware,
    errorMiddleware,
    photoMiddleware
} from './middleware';

export const app = express();

// Add morgan logging
app.use(morgan('common'));

// Add helmet for express.js best-practice security headers
app.use(helmet());

// Add compression to all responses
app.use(compression());

app.use(cors({
    origin: process.env.FRONTEND_HOST
}));

// add the /photos controllers
app.get('/photos/:photoId', photoMiddleware.getPhoto);
app.get('/photos', photoMiddleware.getPhotos);
app.post('/photos',
    uploadMiddleware.parseRequest,
    photoMiddleware.postPhoto
);

// add the error and default case handling middleware
app.all('*', errorMiddleware.default);
app.use(errorMiddleware.errors);
