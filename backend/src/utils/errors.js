import {
    internalServerErrorResponse,
    fileNotFoundResponse,
    methodNotAllowedResponse,
    unprocessableEntityResponse
} from '../config';

export class GeneralError extends Error {

    constructor(message) {
        super();
        this.message = message;
    }

    getResponse() {
        return internalServerErrorResponse;
    }

    getResponseStatus() {
        return internalServerErrorResponse.status;
    }
}

export class NotFoundError extends GeneralError {
    getResponse() {
        return fileNotFoundResponse;
    }
    getResponseStatus() {
        return fileNotFoundResponse.status;
    }
}

export class MethodNotAllowedError extends GeneralError {
    getResponse() {
        return methodNotAllowedResponse;
    }
    getResponseStatus() {
        return methodNotAllowedResponse.status;
    }
}

export class UnprocessableEntityError extends GeneralError {

    constructor(fieldName, message) {
        super(message);
        this.fieldName = fieldName;
    }

    getResponse() {
        return unprocessableEntityResponse(this.message);
    }
    getResponseStatus() {
        return 422;
    }
}
