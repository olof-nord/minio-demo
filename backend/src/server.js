import { app } from './app.js';

const port = 3000;

app.listen(port, () => {

    console.log(`backend is listening at http://localhost:${port}`);
})
    .on('error', (err) => {
        if (err.errno === 'EADDRINUSE') {
            console.error(`Port busy! Another application is already using port ${port}`);
        } else {
            console.error(err);
        }
    });
