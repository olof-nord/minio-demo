export const jpgContentType = 'image/jpeg';
export const uploadFieldName = 'photo';
// This is 20 MB in bytes
export const maxUploadSize = 20000000;
export const fileNotFoundResponse = {
    type: 'file-not-found',
    title: 'Not found',
    status: 404,
    detail: 'File not found!'
};
export const methodNotAllowedResponse = {
    type: 'method-not-allowed',
    title: 'Not allowed',
    status: 405,
    detail: 'HTTP method not allowed!'
};
export const unprocessableEntityResponse = (errorDetail = '') => ({
    type: 'unprocessable-entity',
    title: 'Unprocessable Entity',
    status: 422,
    detail: `Unprocessable entity. ${errorDetail}`
});
export const internalServerErrorResponse = {
    type: 'server-error',
    title: 'Server error',
    status: 500,
    detail: 'Internal server error!'
};
