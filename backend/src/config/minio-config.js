export const photoLocation = 'src/resources/photos';
export const initialPhotoIds = [
    '08b9a097-a6a4-46ae-b2e2-897a004f7fd9',
    '12527dc6-850c-4cec-9dd6-4e6a756a779b',
    '176f9446-f5ea-4df7-9ec7-b25079c6e58d'
];
export const photoBucket = 'photos';
