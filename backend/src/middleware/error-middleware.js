import { MulterError } from 'multer';

import { internalServerErrorResponse } from '../config';
import { GeneralError, UnprocessableEntityError } from '../utils/errors';

const generalErrorHandler = async (err, req, res, next) => {

    if (err instanceof GeneralError) {
        return res.status(err.getResponseStatus()).json(err.getResponse());
    } else if (err instanceof MulterError) {
        // For all possible multer errors, see https://github.com/expressjs/multer/blob/master/lib/multer-error.js
        const unprocessableError = new UnprocessableEntityError(err.field, err.message);

        return res.status(unprocessableError.getResponseStatus()).json(unprocessableError.getResponse());
    }

    return res.status(internalServerErrorResponse.status).json(internalServerErrorResponse);
};

const defaultHandler = async (req, res, next) => {
    return res.status(internalServerErrorResponse.status).json(internalServerErrorResponse);
};

export const errorMiddleware = {
    errors: generalErrorHandler,
    default: defaultHandler
};
