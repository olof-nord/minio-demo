import { minioService } from '../services';
import { jpgContentType } from '../config';

import { NotFoundError } from '../utils/errors';

/**
*  Handles GET HTTP requests for /photos/{photoId}
*/
const photoGET = async (req, res, next) => {

    if (!req.params) {
        next(new NotFoundError());
    }

    try {
        const photoId = req.params.photoId;
        const photoStream = await minioService.getOne(photoId);

        res.setHeader('Content-Type', jpgContentType);
        res.status(200);
        photoStream.pipe(res);
    } catch (error) {
        next(error);
    }
};

/**
*  Handles GET HTTP requests for /photos
*/
const photosGET = async (req, res, next) => {
    try {
        const photoIds = await minioService.getAllIds();

        res.status(200).json({ photoIds: photoIds });
    } catch (error) {
        next(error);
    }
};

/**
*  Handles POST HTTP requests for /photos
*/
const photoPOST = async (req, res, next) => {
    try {
        const photoId = await minioService.putOne(req.file.buffer);

        res.status(201).json({ photoId: photoId });
    } catch (error) {
        next(error);
    }
};

export const photoMiddleware = {
    getPhoto: photoGET,
    getPhotos: photosGET,
    postPhoto: photoPOST
};
