import multer from 'multer';
import path from 'path';

import { uploadFieldName, maxUploadSize } from '../config';

// https://expressjs.com/en/resources/middleware/multer.html

const jpgFileFilter = (req, file, cb) => {

    // Allowed file extensions
    // only jpeg for now
    const filetypes = /jpeg|jpg/;

    // Check file extension
    const extname = filetypes.test(
        path.extname(file.originalname).toLowerCase()
    );

    // Check mime type
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: JPEG images only!');
    }
}

const multerInstance = multer({
    storage: multer.memoryStorage(),
    limits: {
        fileSize: maxUploadSize
    },
    fileFilter: jpgFileFilter
});

const parsePhotoRequest = multerInstance.single(uploadFieldName);

export const uploadMiddleware = {
    parseRequest: parsePhotoRequest
};
