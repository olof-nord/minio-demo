import { Client } from 'minio';
import { v4 as uuid } from 'uuid';

import { NotFoundError, GeneralError } from '../utils/errors';
import { photoBucket } from '../config';

const minioClient = new Client({
    endPoint: process.env.MINIO_HOST,
    port: 9000,
    useSSL: false,
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY
});

const createBucket = async () => {
    try {
        return await minioClient.makeBucket(photoBucket, "local");
    } catch (error) {
        throw new Error(`Bucket ${photoBucket} could not be created!`, error.code);
    }
};

const bucketExists = async () => {
    return await minioClient.bucketExists(photoBucket);
};

const getObject = async (photo) => {
    try {
        return await minioClient.getObject(photoBucket, photo);
    } catch (error) {
        throw new NotFoundError(`${photo} not found!`, error.code);
    }
};

const getObjects = async () => {
    const bucketObjects = await readableStreamToArray(
        minioClient.listObjects(photoBucket)
    );

    return bucketObjects.map((object) => object.name);
};

const readableStreamToArray = async (readableStream) => {
    let result = [];

    for await (const chunk of readableStream) {
        result.push(chunk);
    }

    return result;
}

const putObject = async (photoFile, photoId = uuid()) => {
    try {
        await minioClient.putObject(photoBucket, photoId, photoFile);
        return photoId;
    } catch (error) {
        throw new GeneralError(`${photoFile.name} not uploaded!`, error.code);
    }
};

export const minioService = {
    verifyInitialBucketExists: bucketExists,
    createInitialBucket: createBucket,
    getOne: getObject,
    getAllIds: getObjects,
    putOne: putObject
};
