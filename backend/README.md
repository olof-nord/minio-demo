# Backend

This is the backend sub-project of this minio-demo project.

## Development

The frontend runs on port 3000.

To start locally, note the `nvm` instruction in the main readme to install node and npm.

Once this is finished, the local dependencies needs to be installed once with the below command.

```
npm install
```

Finally to start issue the following command.

```
npm run start
```

### min.io

Min.io is used as a photo storage in this case.
It could work well for any type of binary data though, such as music or documents.

The min.io backend comes with a web-UI which can be accessed at [localhost:9000](http://localhost:9000).
The access and secret key required for logging in are listed in the [docker-compose](../docker-compose.yml) file.

Note that the minio backend needs to be running for development as well. It can be started through the docker-compose with the following.

```bash
docker-compose up -d minio
```
