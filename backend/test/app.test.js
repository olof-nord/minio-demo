import { describe, expect, test, beforeAll } from '@jest/globals';
import request from 'supertest';
import { v4 as uuid } from 'uuid';

import { app } from '../src/app.js';
import { testingService } from './utils/testing-service';
import {
    initialPhotoIds,
    fileNotFoundResponse,
    unprocessableEntityResponse,
    internalServerErrorResponse,
    jpgContentType,
    photoLocation,
    uploadFieldName
} from '../src/config';

describe("Test the photo GET endpoints", () => {

    beforeAll(async () => {
        await testingService.initialUpload();
    });

    test('Fetching all photo ids should return 200 and an array with at least the starting photo ids', async () => {
        const res = await request(app)
            .get('/photos');

        expect(res.status).toBe(200);
        expect(res.body.photoIds).toEqual(
            expect.arrayContaining(initialPhotoIds)
        )
    });

    test('Fetching a photo with an non-existent photo id should return 404', async () => {
        const res = await request(app)
            .get('/photos/' + uuid());

        expect(res.status).toBe(404);
        expect(res.body).toEqual(fileNotFoundResponse);
    });

    test('Fetching a photo with an existent photo name should return 200', async () => {
        const res = await request(app)
            .get('/photos/' + initialPhotoIds[0]);

        expect(res.status).toBe(200);
        expect(res.get('Content-Type')).toEqual(jpgContentType);
    });

});

describe("Test the photo POST endpoints", () => {

    test('Uploading a photo without the required field should return 500', async () => {
        const res = await request(app)
            .post('/photos')
            .type('form');

        expect(res.status).toBe(500);
        expect(res.body).toEqual(internalServerErrorResponse);
    });

    test('Uploading a photo with an incorrect field name should return 422', async () => {
        const res = await request(app)
            .post('/photos')
            .type('form')
            .attach('incorrect-field-name', `${photoLocation}/negative-space-garden-daisy.jpg`);

        expect(res.status).toBe(422);
        expect(res.body).toEqual(unprocessableEntityResponse('Unexpected field'));
    });

    test('Uploading a photo with the required field should return 201', async () => {
        const res = await request(app)
            .post('/photos')
            .type('form')
            .attach(uploadFieldName, `${photoLocation}/negative-space-garden-daisy.jpg`);

        expect(res.status).toBe(201);
    });

});
