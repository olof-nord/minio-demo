import { describe, expect, test, jest, beforeEach, afterEach } from '@jest/globals';

import { photoMiddleware } from '../../src/middleware';
import { minioService } from '../../src/services';

import { NotFoundError } from '../../src/utils/errors';
import { jpgContentType } from '../../src/config';
import {
    mockGetRequest,
    mockPostRequest,
    mockResponse,
    mockNext
} from '../utils/mocks';

describe("Test the photo GET endpoints", () => {

    let minioGetOneMock = null;
    let minioGetAllMock = null;

    beforeEach(() => {
        minioGetOneMock = jest.spyOn(minioService, "getOne");
        minioGetAllMock = jest.spyOn(minioService, "getAllIds").mockReturnValue([]);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    test('Fetching all photo ids should return 200 and an array', async () => {
        const req = mockGetRequest();
        const res = mockResponse();
        const next = mockNext();

        await photoMiddleware.getPhotos(req, res, next);

        expect(minioGetOneMock).toBeCalledTimes(0);
        expect(minioGetAllMock).toBeCalledTimes(1);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({ "photoIds": [] });
    });

    test('Fetching a photo without the required parameter should throw NotFoundError', async () => {
        const req = mockGetRequest();
        const res = mockResponse();
        const next = mockNext();

        await photoMiddleware.getPhoto(req, res, next);

        expect(minioGetOneMock).toBeCalledTimes(0);
        expect(minioGetAllMock).toBeCalledTimes(0);

        expect(next).toHaveBeenCalledWith(new NotFoundError());
    });

    test('Fetching a photo with the required parameter should return 200', async () => {
        const req = mockGetRequest({ photoId: 'a5ac9292-c952-4c63-b5cd-e0228e2c554c' });
        const res = mockResponse();
        const next = mockNext();

        await photoMiddleware.getPhoto(req, res, next);

        expect(minioGetOneMock).toBeCalledTimes(1);
        expect(minioGetOneMock).toHaveBeenCalledWith('a5ac9292-c952-4c63-b5cd-e0228e2c554c');
        expect(minioGetAllMock).toBeCalledTimes(0);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.setHeader).toHaveBeenCalledWith("Content-Type", jpgContentType);
    });

});

describe("Test the photo POST endpoints", () => {

    let minioPutOneMock = null;

    beforeEach(() => {
        minioPutOneMock = jest.spyOn(minioService, "putOne").mockReturnValue('');
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    test('Uploading a photo with the required field should return 201', async () => {
        const req = mockPostRequest();
        const res = mockResponse();
        const next = mockNext();

        await photoMiddleware.postPhoto(req, res, next);

        expect(minioPutOneMock).toBeCalledTimes(1);
        expect(res.status).toHaveBeenCalledWith(201);
    });

});
