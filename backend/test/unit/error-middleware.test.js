import { describe, expect, test } from '@jest/globals';

import { errorMiddleware } from '../../src/middleware';
import {
    NotFoundError,
    MethodNotAllowedError,
    GeneralError
} from '../../src/utils/errors';
import {
    fileNotFoundResponse,
    methodNotAllowedResponse,
    internalServerErrorResponse
} from '../../src/config';

import {
    mockGetRequest,
    mockResponse,
    mockNext
} from '../utils/mocks';

describe("Test the error middleware", () => {

    test('If used with Error, should return status 500', async () => {
        const err = new Error();
        const req = mockGetRequest();
        const res = mockResponse();
        const next = mockNext();

        await errorMiddleware.errors(err, req, res, next);

        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith(internalServerErrorResponse);
    });

    test('If used with GeneralError, should return status 500', async () => {
        const err = new GeneralError();
        const req = mockGetRequest();
        const res = mockResponse();
        const next = mockNext();

        await errorMiddleware.errors(err, req, res, next);

        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith(internalServerErrorResponse);
    });

    test('If used with NotFoundError, should return status 404', async () => {
        const err = new NotFoundError();
        const req = mockGetRequest();
        const res = mockResponse();
        const next = mockNext();

        await errorMiddleware.errors(err, req, res, next);

        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).toHaveBeenCalledWith(fileNotFoundResponse);
    });

    test('If used with MethodNotAllowedError, should return status 405', async () => {
        const err = new MethodNotAllowedError();
        const req = mockGetRequest();
        const res = mockResponse();
        const next = mockNext();

        await errorMiddleware.errors(err, req, res, next);

        expect(res.status).toHaveBeenCalledWith(405);
        expect(res.json).toHaveBeenCalledWith(methodNotAllowedResponse);
    });

});
