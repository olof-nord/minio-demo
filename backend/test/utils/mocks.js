import { jest } from '@jest/globals';

export const mockGetRequest = (parameters) => ({
    params: parameters
});

export const mockPostRequest = () => {
    const req = {};
    req.file = {};
    req.file.buffer = jest.fn().mockReturnValue(req);

    return req;
};

export const mockResponse = () => {
    const res = {};
    res.status = jest.fn().mockReturnValue(res);
    res.json = jest.fn().mockReturnValue(res);
    res.setHeader = jest.fn().mockReturnValue(res);

    return res;
};

export const mockNext = () => {
    return jest.fn();
};
