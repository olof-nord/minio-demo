import { readdir, readFileSync } from 'fs';

import { minioService } from '../../src/services';
import { photoLocation, initialPhotoIds } from '../../src/config';

const uploadInitialPhotos = async () => {

    const bucketExists = await minioService.verifyInitialBucketExists();

    if (!bucketExists) {
        await minioService.createInitialBucket();
    }

    readdir(photoLocation, (err, photos) => {
        if (err) return console.error(err);

        photos.forEach(async (photo, index) => {
            const photoFile = readFileSync(`${photoLocation}/${photo}`);

            await minioService.putOne(photoFile, initialPhotoIds[index]);
        });
    });
};

export const testingService = {
    initialUpload: uploadInitialPhotos
};
