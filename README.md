# minio-demo

A minimal photo upload/download application, written with express.js in JavaScript with a min.io backend.

## Features

A demo application to show the power of the [min.io](https://min.io/) object storage.

- [x] Allow photo download per GET
- [x] Allow photo upload per POST
- [x] Display uploaded photos
- [ ] Allow photo upload per frontend
- [x] Add backend tests without mocks
- [x] Add backend tests with mocks
- [ ] Add frontend tests without mocks
- [ ] Add frontend tests with mocks

## Setup and run

To build and start the setup, simply use the docker-compose file.

```bash
docker-compose up --build
```

## Development

The API is documented [here](documentation/api/minio-demo.openapi.yaml)

To run without docker, `node` and `npm` is required.
To install the same node.js version which is used in the Dockerfiles, use `nvm install` in the root of the repo followed by `nvm use`.

### Frontend

For more details, see the frontend [readme](frontend/README.md).

### Backend

For more details, see the backend [readme](backend/README.md).

## References

During the development, the following online sources have been of great help

- [min.io: JavaScript Client API Reference](https://docs.min.io/docs/javascript-client-api-reference.html)

- [GitHub: mock-express-request-response](https://github.com/HugoDF/mock-express-request-response)
- [Medium: The hidden power of Jest matchers](https://medium.com/@boriscoder/the-hidden-power-of-jest-matchers-f3d86d8101b0)
- [codewithhugo: A testing guide for Express with request and response mocking/stubbing using Jest or sinon](https://codewithhugo.com/express-request-response-mocking/)
- [pawelgrzybek: Mocking functions and modules with Jest](https://pawelgrzybek.com/mocking-functions-and-modules-with-jest/)
- [YouTube: Unit Testing Express Middleware / TDD with Express and Mocha](https://www.youtube.com/watch?v=BwNMUVzo3vs)
- [Medium: Jest matching objects in array](https://medium.com/@andrei.pfeiffer/jest-matching-objects-in-array-50fe2f4d6b98)
- [2ality: Easier Node.js streams via async iteration](https://2ality.com/2019/11/nodejs-streams-async-iteration.html)
- [afteracademy: File Upload with Multer in Node.js and Express](https://afteracademy.com/blog/file-upload-with-multer-in-nodejs-and-express)
- [bezkoder: Upload & resize multiple images in Node.js using Express, Multer, Sharp](https://bezkoder.com/node-js-upload-resize-multiple-images/)
- [Medium: Better Error Handling in Express.js](https://codeburst.io/better-error-handling-in-express-js-b118fc29e9c7)
- [Vue Community](https://vue-community.org/guide/)



Repository icon made by [Good Ware](https://www.flaticon.com/authors/good-ware) found at [Flaticon](https://www.flaticon.com).
