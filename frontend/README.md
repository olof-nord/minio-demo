# Frontend

This is the frontend sub-project of this minio-demo project.

## Development

The frontend runs on port 8080.

To start locally, note the `nvm` instruction in the main readme to install node and npm.

Once this is finished, the local dependencies needs to be installed once with the below command.

```
npm install
```

Finally to start issue the following command.

```
npm run serve
```
