import HttpClient from "./http-service";

class UploadService {
    async postPhoto(file) {
        let formData = new FormData();

        formData.append("photo", file);

        try {
            const response = await HttpClient.post("/photos", formData, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            });

            if (response.status === 201) {
                return response.data;
            }
        } catch (err) {
            throw new Error("Problem with postPhoto", err);
        }
    }

    async getPhotos() {
        try {
            const response = await HttpClient.get("/photos");

            if (response.status === 200) {
                const photoIds = response.data.photoIds;

                const photoURLs = photoIds.map((photoId) => {
                    return `${process.env.VUE_APP_BACKEND_HOST}/photos/${photoId}`;
                });

                return photoURLs;
            }
        } catch (err) {
            throw new Error("Problem with getPhotos", err);
        }
    }

    async getPhoto(photoId) {
        try {
            const response = await HttpClient.get(`/photos/${photoId}`);

            if (response.status === 200) {
                return response.data;
            }
        } catch (err) {
            throw new Error("Problem with getPhoto", err);
        }
    }
}

export default new UploadService();
