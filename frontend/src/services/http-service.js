import axios from 'axios';

const HttpClient = axios.create({
    baseURL: process.env.VUE_APP_BACKEND_HOST,
    headers: {
        "Content-type": "application/json"
    }
});

export default HttpClient;
